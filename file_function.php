<?php
    $path = "/abcd/efgh/File.jpg";
    echo basename($path);

    echo "<br>";

    echo dirname($path);

    echo "<br>";

    //Write mode
    $fp = fopen("Mytext.txt","w+");
    fwrite($fp, "It's raining!");
    fclose($fp);

    //Read mode
    $fp = fopen("Mytext.txt", "r") or die("Unable to open file!");
    echo fread($fp,filesize("Mytext.txt"));
    fclose($fp);

    //Apend mode
    $fp = fopen("Mytext.txt","a+");
    fwrite($fp, " When will your classes end?");
    fclose($fp);

    //Shows the path info of the file
    echo "<br>";
    print_r(pathinfo("/testweb/test.txt"));

    //Deleting a file
    echo "<br>";
    $fp = "Mytext.txt";
    if (!unlink($fp))
    {
        echo ("Error deleting $fp");
    }
    else
    {
        echo ("Deleted $fp");
    }
?>